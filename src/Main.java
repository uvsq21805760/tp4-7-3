
public class Main {

	public static void main(String[] args) {

		Fichier f1 = new Fichier("video", 12);
		Fichier f2 = new Fichier("java", 4);
		Fichier f3 = new Fichier("pdf", 50);
		Fichier f4 = new Fichier("image",52);
		Fichier f5 = new Fichier("film", 42);
		
		Repertoires r1 = new Repertoires("dossier1");
		Repertoires r2 = new Repertoires("dossier2");
		
		r2.addObj(f1);
		r2.addObj(f2);
		r2.addObj(f3);
		
		
		r1.addObj(r2);
		r1.addObj(f4);
		
		System.out.println("la taille du doosier 2 est :"+r2.getTaille());
		System.out.println("la taille du doosier 1 est :"+r1.getTaille());
		
		r1.addObj(r1);
		r2.addObj(r1);
		
		
	}

}
